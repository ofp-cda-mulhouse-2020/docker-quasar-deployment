FROM debian:latest
RUN apt-get update
RUN apt install -y wine64 android-sdk android-sdk-platform-23 nodejs npm wget curl
RUN npm install --global yarn && yarn global add @quasar/cli
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-6858069_latest.zip && \
  unzip commandlinetools-linux-6858069_latest.zip
RUN yes | ./cmdline-tools/bin/sdkmanager --sdk_root=/usr/lib/android-sdk/ --licenses
